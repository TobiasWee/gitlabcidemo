package com.mangroo.temperature.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mangroo.temperature.business.TemperatureService;
import com.mangroo.temperature.data.Temperature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(MockitoJUnitRunner.class)
public class TemperatureControllerTest {

    Logger logger = LoggerFactory.getLogger(TemperatureControllerTest.class);

    private MockMvc mockMvc;

    @InjectMocks
    private TemperatureController temperatureController;

    @Mock
    private TemperatureService temperatureService;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.standaloneSetup(temperatureController)
                .build();
    }

    @Test
    public void saveTemperatureReturnsHttpStatusCreated() throws Exception {
        Temperature temperatureToSave = createTemperature();
        given(temperatureService.save(any())).willReturn(temperatureToSave);

        String jsonString = asJsonString(temperatureToSave);
        logger.info("JSON to save: {}", jsonString);
        MockHttpServletResponse response = mockMvc.perform(post("/temperatureReading/temperatures")
                .content(jsonString)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
    }

    private Temperature createTemperature() {
        return Temperature.builder()
                .name("ControllerTest")
                .temperatureReading(25)
                .timestamp(new Date())
                .build();
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
